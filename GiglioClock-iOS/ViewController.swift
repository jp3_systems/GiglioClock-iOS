//
//  ViewController.swift
//  GiglioClock-iOS
//
//  Created by Joseph Peluso III on 8/26/17.
//  Copyright © 2017 jp3.systems. All rights reserved.
//

import UIKit
import AVFoundation

class SettingsViewController: UIViewController {
    @IBOutlet weak var nextFeastDate: UIDatePicker!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    var endDate = UserDefaults.standard.object(forKey: "endDate") as? Date
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextFeastDate.date = endDate!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if sender as AnyObject? === saveButton {
           UserDefaults.standard.set(nextFeastDate.date, forKey: "endDate")
        }
    }

}

class MainViewController: UIViewController {
    
    @IBOutlet weak var TimeLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBAction func DaysUntilFeast(_ sender: Any) {
        daysUntilFeast()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getTheNow), userInfo: nil, repeats: true)
        if UserDefaults.standard.object(forKey: "endDate") == nil {
            UserDefaults.standard.set(Date(), forKey: "endDate")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func unwindToMain(sender: UIStoryboardSegue){
    }
    
    func getTheNow() {
        let formatter = DateFormatter()
        let formatter1 = DateFormatter()
        formatter.dateStyle = .full
        formatter1.dateFormat = "h:mm"
        let now = Date()
        let date_string = formatter.string(from:now)
        let time_string = formatter1.string(from:now)
        TimeLabel.text = time_string
        DateLabel.text = date_string
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "H"
        let setTime = formatter2.string(from: Date())
        if Int(setTime)! >= 19 || Int(setTime)! <= 7 {
            self.view.backgroundColor = UIColor(red: 25/255, green: 25/255, blue: 112/255, alpha: 1)
            DateLabel.textColor = UIColor(red: 0/255, green: 191/255, blue: 255/255, alpha: 1)
            TimeLabel.textColor = UIColor(red: 0/255, green: 191/255, blue: 255/255, alpha: 1)
        } else {
            self.view.backgroundColor = UIColor(red: 0/255, green: 191/255, blue: 255/255, alpha: 1)
            DateLabel.textColor = UIColor(red: 25/255, green: 25/255, blue: 112/255, alpha: 1)
            TimeLabel.textColor = UIColor(red: 25/255, green: 25/255, blue: 112/255, alpha: 1)
        }
    }
    
    func daysUntilFeast() {
        let endDate: Date = UserDefaults.standard.object(forKey: "endDate") as! Date
        let now = Date()
        let daysUntil = Calendar.current.dateComponents([.day], from: now, to: endDate).day!
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy"
        let nextYear = formatter2.string(from: endDate)
        let myMessage = daysUntilMessage(x: daysUntil, y: nextYear)
        playSound()
        let alertController = UIAlertController(title: "Buona Festa!", message: myMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Set Date", style: UIAlertActionStyle.destructive, handler: {
            (action) -> Void in self.performSegue(withIdentifier: "segueToSettings", sender: UIAlertAction.self)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func daysUntilMessage(x: Int, y: String) -> String {
        let du1 = [50,36,69,78,93]
        let du2 = [125,165,192,109,143]
        if x < -13 {
            return "The \(y) feast is over!  Update the date in the settings for next year!"
        } else if x < 0 {
            return "The feast is here, buona festa!"
        } else if x == 0 {
            return "The feast starts today! Go Go Go!!!"
        } else if x == 1 {
            return "OMG the feast starts TOMORROW!!!"
        } else if x < 10 {
            return "\(x) days left!!!"
        } else if du1.contains(x) {
            return "Yippie!! Only \(x) days left until the \(y) feast!"
        } else if du2.contains(x) {
            return "Oh Man! The \(y) feast is \(x) days away!"
        } else if x == 100 {
            return "YAY! \(x) more days!"
        } else if (x == 200) || (x == 300) {
            return "Ugh, still \(x) days left!"
        } else if x == 124 {
            return "Hey look, there's \(x) days left until the \(y) feast!"
        } else if x == 219 {
            return "The \(y) feast is \(x) days away!"
        } else {
            return "There are \(x) days until the \(y) feast!"
        }
    }
    
    var player: AVAudioPlayer?
    var ran = 0
    var sFile: String = "buona_festa"
    
    func playSound() {
        if ran == 0 {
            sFile = "buona_festa"
            ran = 1
        } else if ran == 1 {
            sFile = "quando_festa"
            ran = 2
        } else if ran == 2 {
            sFile = "commands"
            ran = 0
        }
        guard let url = NSDataAsset(name: sFile) else { print("asset not found") ; return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(data: url.data, fileTypeHint: AVFileTypeMPEGLayer3)
            guard let player = player else { return }
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
