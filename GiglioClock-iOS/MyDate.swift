//
//  MyDate.swift
//  GiglioClock-iOS
//
//  Created by Alba Quijije-Peluso on 8/26/17.
//  Copyright © 2017 jp3.systems. All rights reserved.
//

import Foundation

class MyDate: NSObject, NSCoding {
    var endDate: Date?
    
    static let Dir = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = Dir.appendingPathComponent("endDate")
    
    // Name:String? optional because it could be nil
    required init(endDate:Date?) {
        self.endDate = endDate
    }
    
    func encode(with aCoder: NSCoder) {
        // in Swift 3 the .encodeObject() method is updated to encode (object: Any?)
        aCoder.encode(endDate, forKey: "endDate")
    }
    
    required init?(coder aDecoder: NSCoder) {
        // Slight update modification of Jose Guerrero's post where as! was used
        // note in Swift 3, xcode 8.2.1 the as! is required to be as?
        self.endDate = aDecoder.decodeObject(forKey: "endDate") as? Date
    }
}
